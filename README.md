#django_app_installer


##Introduction:


simple script to setup a django application with nginx and gunicorn, assuming that you used django 1.5 or later and virtualenv requirements.txt specifications.


for and example of an application using this files layout you can check the default application in the configuration on https://github.com/Chedi/CarSharing


To use it, simply clone this repo and edit the setup.sh script and modify one or more of this variables:
* **SERVER_NAME**
* **GUNICORN_WORKERS**
* **APPLICATION_NAME**
* **APPLICATION_REPO**
* **SSL_SUBJECT**


The meangin of these variables are quite obvious, but having to write the documentation any way being pedentic is a must

```
SERVER_NAME="localhost"
```
the server name as used in the gunicorn virtual host configuration


```
GUNICORN_WORKERS="4"
```
the number of workers used by the gunicorn script, as a rule of thumb (no scientific explanation for this), use twice the number of cores on your system.


```
APPLICATION_NAME="Mitfahrgelegenheit"
```
the name of the django project you are using, this is a very important variable because:
* the application repo will be cloned in a folder having this name
* this must be the same name of the default django app inside your project because it will be used in the gunicorn script for the definition of the setting and wsgi python modules


```
APPLICATION_REPO="https://github.com/Chedi/CarSharing.git"
```
The repo of your django application, notice that the structure layout have changed since django 1.5 so please do no use this against old project.


```
SSL_SUBJECT="/C=TN/ST=Tunis/L=Tunis/O=Epsilon tech/OU=R&D/CN=localhost/emailAddress=contact@epsilon.tn"
```
This is the subject used by openssl for the dummy ssl generation.


## System dependencies:


Befor using this script just make sure you have this packages installed on your system:

* python
* python-virtualenv
* openssl
* supervisor
* nginx
* git


Also the user must be in the sudoers list to be able to overwrite the config file and restart both nginx and supervisord

## Todo:


* For now the script have been tested only in redhat/fecora/centos, the configuration paths and files may differ on other system (yes I'm looking at you debian), so I will try to get this verified as soon as possible.

* It would be also great to have the script install the system dependencies or at least verify them before continuing (work in progress

* Adding some control parameter for limiting the script action to certain task will also be great, for example only regenerate the nginx config and not all the scripts

* Checking the results of each task and controling the messages verbosity

* Adding extra method to pass the configuration variables to the script (via environment, config file or command line)
