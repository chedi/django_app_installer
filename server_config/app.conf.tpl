upstream upstream_server {
  server %GUNICORN_SOCKET% fail_timeout=0;
}

server {
    listen 80;
    server_name %SERVER_NAME%;
    rewrite ^ https://$server_name$request_uri? permanent;
}

server {
    listen 443;
    server_name %SERVER_NAME%;

    ssl on;
    ssl_certificate %SSL_CRT%;
    ssl_certificate_key %SSL_KEY%;

    error_log  %LOGS_DIR%nginx-error.log;
    access_log %LOGS_DIR%nginx-access.log;

    keepalive_timeout 5;
    client_max_body_size 4G;

    location /static/ {
        alias   %STATIC_DIR%;
    }

    location /media/ {
        alias   %MEDIA_DIR%;
    }

    location / {
        proxy_redirect   off;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-HTTPS on;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

        if (!-f $request_filename) {
            proxy_pass http://upstream_server;
            break;
        }
    }

    error_page 500 502 503 504 /500.html;
    location = /500.html {
        root %STATIC_DIR%;
    }
}